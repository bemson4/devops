package com.sungardas.custom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.processor.MessageProcessor;

public class CustomProcessor implements MessageProcessor {
	
	
	 @Override
	    public MuleEvent process(MuleEvent event) {     
	        try
	        {
	            MuleMessage inputMessage =  event.getMessage(); 
	            String origpayload =inputMessage.getPayloadAsString();
	            String re1=".*(&lt;CustomerCode&gt;)";	// Tag 1
	     	    String re2="(\\d+)";	// Integer Number 1
	     	    String re3="(&lt;\\/CustomerCode&gt;).*";	// Tag 2
	     	    
	     	   String re4=".*(&lt;FlexibleCodeField fieldId=\"5\"&gt;)";	// Tag 1
	     	   String re5="(\\d+)";	// Integer Number 1
	   	       String re6="(&lt;\\/FlexibleCodeField&gt;).*";	// Tag 2
	    	    
	    	    Pattern p = Pattern.compile(re1+re2+re3,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	    	    Matcher m = p.matcher(origpayload);
	    	    String customerCode="NOTFOUND";
	    	    if (m.find())
	    	    {
	    	        //String tag1=m.group(1);
	    	        String int1=m.group(2);//extracting customer code
	    	        //String tag2=m.group(3);
	    	        customerCode=int1.toString();
	    	    }
	    	    p = Pattern.compile(re4+re5+re6,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	    	    m = p.matcher(origpayload);
	    	    String division="NOTFOUND";
	    	    if (m.find())
	    	    {
	    	        //String tag1=m.group(1);
	    	        String int1=m.group(2);
	    	        //String tag2=m.group(3);
	    	        division=int1.toString();
	    	    }
	    	    StringBuffer flexField2=new StringBuffer("&lt;FlexibleCodeField fieldId=\"2\"&gt;");
	    	    flexField2.append(customerCode);
	    	    flexField2.append("&lt;/FlexibleCodeField&gt;");
	    	    //Replacing the division tag to match the flexfield value, because Aria does not have capability to change division field for Candian customers
	    	    StringBuffer divisionTag= new StringBuffer("&lt;Division&gt;");
	    	    divisionTag.append(division);
	    	    divisionTag.append("&lt;/Division&gt;");
	    	    origpayload=origpayload.replaceAll("fieldId=\"4\"", "fieldId=\"10\"");
	    	    origpayload=origpayload.replaceAll("fieldId=\"5\"", "fieldId=\"15\"");
	    	    origpayload=origpayload.replaceAll("&lt;FlexibleCodeField fieldId=\"1\"&gt;CUSTOMER_COMPANY_NAME&lt;/FlexibleCodeField&gt;", ("&lt;FlexibleCodeField fieldId=\"1\"&gt;CUSTOMER_COMPANY_NAME&lt;/FlexibleCodeField&gt;"+flexField2.toString()));
	    	    origpayload=origpayload.replace("&lt;Division&gt;01&lt;/Division&gt;", divisionTag.toString());
	            inputMessage.setPayload(origpayload);
	        }
	        
	        catch(Exception e){
	            e.printStackTrace();            
	        }       
	        return event ;
	    }
	

}
