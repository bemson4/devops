package com.sungardas.custom;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

public class MessageProcess extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage arg0, String arg1)
			throws TransformerException {
		
		arg0.removeProperty("SOAPAction", PropertyScope.INBOUND);
		return arg0; 
	}
	

}
